"""Blueprint Library plugin for Alliance Auth."""

# pylint: disable = invalid-name
default_app_config = "blueprints.apps.BlueprintsConfig"

__version__ = "1.7.0"
__title__ = "Blueprints"
